﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace MatrixPermutation {

    public static class Matrix {

        public static bool IsPowerOfTwo(int n) {
            return n >= 2 && (n & (n - 1)) == 0;
        }

        public static int GetNewMatrixDimension(int n) {
            int pow = 0;
            while (n > 0) {
                n >>= 1;
                pow++;
            }
            return 1 << pow;
        }

        public static double[,] CreateMatrixWithNewDemension(double[,] matrix, int n) {
            var m = GetNewMatrixDimension(n);
            var resultMatrix = new double[m, m];
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    if (i >= n || j >= n)
                        resultMatrix[i, j] = 0;
                    else
                        resultMatrix[i, j] = matrix[i, j];
            return resultMatrix;
        }

        public static double[,] PreShtrassenPermutation(double[,] matrixA, double[,] matrixB, int n) {
            if (IsPowerOfTwo(n))
                return ShtrassenPermutation(matrixA, matrixB, n);
            else {
                return ShtrassenPermutation(CreateMatrixWithNewDemension(matrixA, n), CreateMatrixWithNewDemension(matrixB, n), GetNewMatrixDimension(n));
            }


        }
        public static void SplitMatrix(double[,] matrix, double[,] m11, double[,] m12, double[,] m21, double[,] m22, int m) {
            int n = m << 1;
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    m11[i, j] = matrix[i, j];
            for (int i = 0; i < m; i++)
                for (int j = m; j < n; j++)
                    m12[i, j - m] = matrix[i, j];
            for (int i = m; i < n; i++)
                for (int j = 0; j < m; j++)
                    m21[i - m, j] = matrix[i, j];
            for (int i = m; i < n; i++)
                for (int j = m; j < n; j++)
                    m22[i - m, j - m] = matrix[i, j];
        }

        public static double[,] CollectMatrix(double[,] m11, double[,] m12, double[,] m21, double[,] m22, int m) {
            int n = m << 1;
            var resultMatrix = new double[n, n];
            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    resultMatrix[i, j] = m11[i, j];
            for (int i = 0; i < m; i++)
                for (int j = m; j < n; j++)
                    resultMatrix[i, j] = m12[i, j - m];
            for (int i = m; i < n; i++)
                for (int j = 0; j < m; j++)
                    resultMatrix[i, j] = m21[i - m, j];
            for (int i = m; i < n; i++)
                for (int j = m; j < n; j++)
                    resultMatrix[i, j] = m22[i - m, j - m];
            return resultMatrix;
        }

        public static double[,] MatrixAddition(double[,] matrixA, double[,] matrixB, int n) {
            var resultMatrix = new double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    resultMatrix[i, j] = matrixA[i, j] + matrixB[i, j];
            return resultMatrix;
        }

        public static double[,] MatrixSubtraction(double[,] matrixA, double[,] matrixB, int n) {
            var resultMatrix = new double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    resultMatrix[i, j] = matrixA[i, j] - matrixB[i, j];
            return resultMatrix;
        }
        public static double[,] ShtrassenPermutation(double[,] matrixA, double[,] matrixB, int n) {
            if (n <= 32)
                //  return new double[1, 1] { { matrixA[0, 0] * matrixB[0, 0] } };
                return SimpleMatrixPermutation(matrixA, matrixB, n);
            else {
                var m = n >> 1;
                var a11 = new double[m, m];
                var a12 = new double[m, m];
                var a21 = new double[m, m];
                var a22 = new double[m, m];
                var b11 = new double[m, m];
                var b12 = new double[m, m];
                var b21 = new double[m, m];
                var b22 = new double[m, m];
                SplitMatrix(matrixA, a11, a12, a21, a22, m);
                SplitMatrix(matrixB, b11, b12, b21, b22, m);

                var p1 = ShtrassenPermutation(MatrixAddition(a11, a22, m), MatrixAddition(b11, b22, m), m);
                var p2 = ShtrassenPermutation(MatrixAddition(a21, a22, m), b11, m);
                var p3 = ShtrassenPermutation(a11, MatrixSubtraction(b12, b22, m), m);
                var p4 = ShtrassenPermutation(a22, MatrixSubtraction(b21, b11, m), m);
                var p5 = ShtrassenPermutation(MatrixAddition(a11, a12, m), b22, m);
                var p6 = ShtrassenPermutation(MatrixSubtraction(a21, a11, m), MatrixAddition(b11, b12, m), m);
                var p7 = ShtrassenPermutation(MatrixSubtraction(a12, a22, m), MatrixAddition(b21, b22, m), m);

                var c11 = MatrixAddition(MatrixAddition(p1, p4, m), MatrixSubtraction(p7, p5, m), m);
                var c12 = MatrixAddition(p3, p5, m);
                var c21 = MatrixAddition(p2, p4, m);
                var c22 = MatrixAddition(MatrixSubtraction(p1, p2, m), MatrixAddition(p3, p6, m), m);
                return CollectMatrix(c11, c12, c21, c22, m);

            }


        }
        public static void PrintMatrix(double[,] resultMatrix, int n) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++)
                    Console.Write("{0,-16:0.00}", resultMatrix[i, j]);
                Console.WriteLine();
            }
            Console.WriteLine();

        }

        public static void PreProgram() {
            string[] data = File.ReadAllLines("test5.txt");
            var n = int.Parse(data[0]);
            var matrixA = new double[n, n];
            var matrixB = new double[n, n];
            for (int i = 0; i < n; i++) {
                double[] massA = data[i + 1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => double.Parse(s)).ToArray();
                double[] massB = data[i + n + 1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => double.Parse(s)).ToArray();
                for (int j = 0; j < n; j++) {
                    matrixA[i, j] = massA[j];
                    matrixB[i, j] = massB[j];
                }
            }
            SimpleMatrixPermutation(matrixA, matrixB, n);
            PreShtrassenPermutation(matrixA, matrixB, n);
        }

        public static void RandomMatrix() {
            var rnd = new Random();
            //   int n = rnd.Next(0, 10);
            int n = 32;
            var matrixA = new double[n, n];
            var matrixB = new double[n, n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrixA[i, j] = rnd.Next(0, 10);
                    matrixB[i, j] = rnd.Next(0, 10);
                }
            }
            var sw1 = new Stopwatch();
            var sw2 = new Stopwatch();
            sw1.Start();
            //   PrintMatrix(SimpleMatrixPermutation(matrixA, matrixB, n), n);
              for (int i = 0; i<100; i++)
            SimpleMatrixPermutation(matrixA, matrixB, n);
            sw1.Stop();
            Console.WriteLine(sw1.ElapsedMilliseconds/100);
            sw2.Start();

            // PrintMatrix(PreShtrassenPermutation(matrixA, matrixB, n), n);
            for (int i = 0; i < 100; i++)
                PreShtrassenPermutation(matrixA, matrixB, n);
            sw2.Stop();

            Console.WriteLine(sw2.ElapsedMilliseconds/100);
        }

        public static void MatrixFromFile() {
            string[] data = File.ReadAllLines("test32.txt");
            var n = int.Parse(data[0]);
            var matrixA = new double[n, n];
            var matrixB = new double[n, n];
            for (int i = 0; i < n; i++) {
                double[] massA = data[i + 1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => double.Parse(s)).ToArray();
                double[] massB = data[i + n + 1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => double.Parse(s)).ToArray();
                for (int j = 0; j < n; j++) {
                    matrixA[i, j] = massA[j];
                    matrixB[i, j] = massB[j];
                }
            }
            var sw1 = new Stopwatch();
            var sw2 = new Stopwatch();
            sw1.Start();
            //   PrintMatrix(SimpleMatrixPermutation(matrixA, matrixB, n), n);
            //  for (int i = 0; i<100; i++)
            SimpleMatrixPermutation(matrixA, matrixB, n);
            sw1.Stop();
            Console.WriteLine(sw1.ElapsedMilliseconds);
            sw2.Start();

            // PrintMatrix(PreShtrassenPermutation(matrixA, matrixB, n), n);
            for (int i = 0; i < 100; i++)
                PreShtrassenPermutation(matrixA, matrixB, n);
            sw2.Stop();

            Console.WriteLine(sw2.ElapsedMilliseconds / 100);
        }

        public static double[,] SimpleMatrixPermutation(double[,] matrixA, double[,] matrixB, int n) {
            var resultMatrix = new double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    for (int r = 0; r < n; r++)
                        resultMatrix[i, j] += matrixA[i, r] * matrixB[r, j];
            return resultMatrix;
        }


    }
    class Program {
        static void Main(string[] args) {
            Matrix.PreProgram();
            Matrix.RandomMatrix();
          //  Matrix.MatrixFromFile();
            Console.ReadKey();
        }
    }
}
