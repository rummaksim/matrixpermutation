﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void TestMethod1() {
            int n = 17;
            var rnd = new Random();
            var A = new double[n, n];
            var B = new double[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    A[i, j] = B[i, j] = rnd.Next(0, 10);
                }
            var C = MatrixPermutation.Matrix.SimpleMatrixPermutation(A, B, n);
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    Assert.AreEqual(C[i,j], MatrixPermutation.Matrix.PreShtrassenPermutation(A, B, n)[i,j]);
        }

    }
}
